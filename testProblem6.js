let carsArray = require('./Inventory.js');

let car = require('./Problem6.js');

let cars = car.bmwNdAudi(carsArray)

let actualOutput = [{ id: 6, car_make: 'Audi', car_model: 'riolet', car_year: 1995 },
    { id: 8, car_make: 'Audi', car_model: '4000CS Quattro', car_year: 1987 },
    { id: 25, car_make: 'BMW', car_model: '525', car_year: 2005 },
    { id: 30, car_make: 'BMW', car_model: '6 Series', car_year: 2010 },
    { id: 44, car_make: 'Audi', car_model: 'Q7', car_year: 2012 },
    { id: 45, car_make: 'Audi', car_model: 'TT', car_year: 2008 }
];

let flag = true;
if (cars.length === actualOutput.length) {
    for (let index = 0; index < actualOutput.length; index++) {
        if (actualOutput[index].id !== cars[index].id && actualOutput[index].car_make !== cars[index].car_make &&
            actualOutput[index].car_model !== cars[index].car_model && actualOutput[index].car_year !== cars[index].car_year) {
            flag = false;
            break;
        }
    }
}

if (flag === true) {
    console.log(cars);
} else {
    console.log('Different Output');
}