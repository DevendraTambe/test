function bmwNdAudi(inventory) {
    let BMW_N_Audi = [];
    for (let i = 0; i < inventory.length; i++) {
        if (inventory[i].car_make === "Audi" || inventory[i].car_make === "BMW") {
            BMW_N_Audi.push(inventory[i]);
        }
    }
    return BMW_N_Audi;
}

module.exports = { bmwNdAudi };