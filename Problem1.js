function findCar(inventory, id) {
    return `Car ${id} is a ${inventory[id].car_year} ${inventory[id].car_make} ${inventory[id].car_model}`;
}
module.exports = { findCar };