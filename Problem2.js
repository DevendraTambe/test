function findLastCar(inventory) {
    let car = inventory[inventory.length - 1];
    return "Last car is a " + car.car_make + " " + car.car_model;
}

module.exports = { findLastCar };