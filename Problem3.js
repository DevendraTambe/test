function carModels(inventory) {
    let sortedCar = sortCars(inventory);

    //console.log(sortedCar);
    return sortedCar;
}

function sortCars(cars) {
    let carIndex = 0,
        nextCarIndex;
    while (carIndex < cars.length) {
        nextCarIndex = carIndex + 1;
        while (nextCarIndex < cars.length) {
            if (cars[nextCarIndex].car_model < cars[carIndex].car_model) {
                let temproary = cars[carIndex].car_model;
                cars[carIndex].car_model = cars[nextCarIndex].car_model;
                cars[nextCarIndex].car_model = temproary;
            }
            nextCarIndex++;
        }
        carIndex++;
    }

    return cars;
}

module.exports = { carModels };